// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse(
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
      status: json['status'] as int,
      message: json['message'] as String);
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
      'status': instance.status,
      'message': instance.message
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
      ref: json['ref'] as String,
      name: json['name'] as String,
      surname: json['surname'] as String,
      email: json['email'] as String,
      contactNumber: json['contact_number'] as String,
      role: json['role'] as String,
      status_since: json['status_since'] as String,
      access_token: json['access_token'] as String,
      token_type: json['token_type'] as String,
      expires_in: json['expires_in'] as int,
      expires_at: json['expires_at'] as int)
    ..status = json['status'] as String;
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'ref': instance.ref,
      'name': instance.name,
      'surname': instance.surname,
      'email': instance.email,
      'contact_number': instance.contactNumber,
      'role': instance.role,
      'status': instance.status,
      'status_since': instance.status_since,
      'access_token': instance.access_token,
      'token_type': instance.token_type,
      'expires_in': instance.expires_in,
      'expires_at': instance.expires_at
    };
