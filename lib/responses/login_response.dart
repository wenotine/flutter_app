import 'package:json_annotation/json_annotation.dart';

part 'login_response.g.dart';

@JsonSerializable()


class LoginResponse{

  Data data;
  int status;
  String message;


  LoginResponse({this.data, this.status, this.message});

  factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);

}

@JsonSerializable()


class Data{



  String ref;
  String name;
  String surname;
  String email;
  @JsonKey(name: 'contact_number')
  String contactNumber;
  String role;
  String status;
  String status_since;
  String access_token;
  String token_type;
  int expires_in;
  int expires_at;

  Data({this.ref, this.name, this.surname, this.email, this.contactNumber, this.role,
       this.status_since, this.access_token, this.token_type,
      this.expires_in, this.expires_at});


    factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

    Map<String, dynamic> toJson() => _$DataToJson(this);



}


