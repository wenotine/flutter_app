import 'package:flutter/material.dart';

class Anim extends StatefulWidget {
  @override
  _AnimState createState() => _AnimState();
}

class _AnimState extends State<Anim> with SingleTickerProviderStateMixin {
  AnimationController animController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();

    animController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);
    animation =
        CurvedAnimation(parent: animController, curve: Curves.easeIn);
    animation.addListener(() {
      this.setState(() {});
    });
    animation.addStatusListener((AnimationStatus status) {});
    animController.repeat();
  }

  @override
  void dispose() {
    super.dispose();
    animController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.deepOrange,
      width: animation.value * 100,
      height: 5.0,
    );
  }
}
