import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:washify_flutter/responses/login_response.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'home.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.Scaffold',
      theme: ThemeData(
        primaryColor: Colors.green[600],
      ),
      home: LoginPage(),
      routes: <String, WidgetBuilder>{
        "/home": (BuildContext context) => Home(),
      },
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);



  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  var formkey = GlobalKey<FormState>();
  var formFields = LoginDetails();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Washify Login Page'),
      ),
      body: Container(child:
          Form(key: formkey, child:
              Padding(padding: EdgeInsets.all(20.0), child:
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Container(padding: EdgeInsets.all(10.0), color: Colors.green[600], child: Column(children: <Widget>[
                  Row(children: <Widget>[
                    Expanded(child:
                    TextFormField(
                      validator: _usernameValidator,
                      onSaved: (String username){
                        formFields.username = username;
                      },
                      cursorColor: Colors.black54,
                      decoration: InputDecoration(hintText:   "Username", fillColor: Colors.white, filled: true),),)
                  ],),
                  Row(children: <Widget>[
                    Expanded(child:
                    TextFormField(
                      validator: _passwordValidator,
                      onSaved: (String password){
                        formFields.password = password;
                      },
                      obscureText: true,
                      decoration: InputDecoration(hintText:   "Password",fillColor: Colors.white, filled: true),),)
                  ],),
                  Padding(padding: EdgeInsets.all(8.0), child:
                  RaisedButton(color: Colors.white, textColor: Colors.green[600],child: Text("Login"), onPressed: _submitForm), ),
                ],),),
              ],)), ),)

    );
  }

  String _usernameValidator(String username) {

    if(username.length == 0){
      return "Please fill the username";
    }
    return null;


  }

  String _passwordValidator(String username) {
    if(username.length == 0){
      return "Please add a password";
    }
    return null;
  }

  void _submitForm(){

    if(formkey.currentState.validate()){

        formkey.currentState.save();

        _dataGoesToServer(formFields.username, formFields.password);
    }



  }

  Future<LoginResponse> _dataGoesToServer(String email, String password) async{

    var url = "http://washify.db.justsomeapps.mobi/api/auth/login";

    await http.post(url,
        body: {"email": email, "password": password})
        .then((response) {

          LoginResponse loginResponse = LoginResponse.fromJson(json.decode(response.body));

          if(loginResponse.status == 1){

            Data data = loginResponse.data;
            _saveToSharedPrefs(data);

            Navigator.of(context).pushNamed("/home");
          }



    });

  }

  Future<void> _saveToSharedPrefs(Data data) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var userObjectString = jsonEncode(data);

    await prefs.setString('user', userObjectString);


  }

}

class LoginDetails{

  var username="";
  var password = "";
}